﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ExempleWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Personne per1 = new Personne();
        public MainWindow()
        {
            DataContext = per1;
            InitializeComponent();
        }

        private void bpok_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(per1.Nom);
        }
    }
}