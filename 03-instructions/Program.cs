﻿namespace _03_instructions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Condition if
            Console.WriteLine("Saisir un nombre entier");

            int val = Convert.ToInt32(Console.ReadLine());
            if (val > 100)
            {
                Console.WriteLine("la valeur est supérieur à 100");
            }
            else if (val == 100)
            {
                Console.WriteLine("la valeur est égale à 100");
            }
            else
            {
                Console.WriteLine("la valeur est inférieure à 100");
            }
            #endregion

            #region Exercice: trie de 2 valeurs
            // Saisir 2 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            if (d1 < d2)
            {
                Console.WriteLine($"{d1}<{d2}");
            }
            else
            {
                Console.WriteLine($"{d2}<{d1}");
            }
            #endregion

            #region Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            Console.Write("Intervalle: Saisir un entier ");
            int vi = Convert.ToInt32(Console.ReadLine());
            if (vi > -4 && vi <= 7)
            {
                Console.WriteLine($"{vi} fait partie de l'interval");
            }
            #endregion

            #region Opérateur ternaire
            Console.Write("Valeur absolue: Saisir une valeur double ");
            int va = Convert.ToInt32(Console.ReadLine());

            int valeurAbsolue = va > 0 ? va : -va;

            Console.WriteLine($"|{va}|={valeurAbsolue}");

            #endregion

            #region condition switch
            const int PREMIER_JOUR = 1;
            Console.Write("Saisir un jour entre 1 et 7 ");
            int jour = Convert.ToInt32(Console.ReadLine());
            switch (jour)
            {
                // la valeur d'un case doit être une constante au moment de la compilation
                case PREMIER_JOUR:    // cela peut être une constante
                case PREMIER_JOUR + 1:
                case 3:               // ou une littéral
                case 4:
                case 5:
                    Console.WriteLine("Semaine");
                    break;
                case 6:
                case 7:
                    Console.WriteLine("Week-end");
                    break;
                default:
                    Console.WriteLine("Erreur de saisie");
                    break;
            }

            // Clause when C# 7.0 
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case var x when x == 6 || x == 7:
                    Console.WriteLine("week-end");
                    break;
            }

            // switch avec les corps d'expression C# 8.0 (.net 6 et .net 8)
            string nomJour = jour switch
            {
                1 => "Lundi",
                2 => "Mardi",
                _ => "Autre jour"
            };
            Console.WriteLine(nomJour);

            #endregion
            #region exercice calculatrice
            double val1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double val2 = Convert.ToDouble(Console.ReadLine());

            switch (op)
            {
                case "+":
                    Console.WriteLine($" {val1} + {val2} = {val1 + val2}");
                    break;
                case "-":
                    Console.WriteLine($" {val1} - {val2} = {val1 - val2}");
                    break;
                case "*":
                    Console.WriteLine($" {val1} * {val2} = {val1 * val2}");
                    break;
                case "/":
                    if (val2 == 0.0)  // (val2>-1e-9 && val2<1e-9)
                    {
                        Console.WriteLine("division par 0");
                    }
                    else
                    {
                        Console.WriteLine($" {val1} / {val2} = {val1 / val2}");
                    }
                    break;
                default:
                    Console.WriteLine($"{op} n'est pas un opérateur valide");
                    break;
            }
            #endregion
            #region boucle while
            int i = 0;
            while (i < 10)
            {
                Console.WriteLine($"i={i}");
                i++;
            }
            #endregion

            #region boucle do while
            i = 0;
            do
            {
                Console.WriteLine($"i={i}");
                i++;
            } while (i < 10);
            #endregion

            #region  boucle for
            for (int j = 0; j < 10; j++)
            {
                Console.WriteLine(j);
            }
            #endregion

            #region break
            for (int j = 0; j < 10; j++)
            {
                if (j == 2)
                {
                    break;  // break => termine la boucle
                }
                Console.WriteLine(j);
            }

            for (int k = 0; ;)
            {
                k++;
                if (k >= 10)
                {
                    break;
                }
                Console.WriteLine(k);
            }
            #endregion

            #region continue
            for (int j = 0; j < 10; j++)
            {
                if (j == 2)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine(j);
            }
            #endregion

            #region goto
            // goto boucle imbriqué
            for (int j = 0; j < 10; j++)
            {
                for (int k = 0; k < 5; k++)
                {
                    if (k == 2)
                    {
                        goto EXIT_LOOP;
                    }
                    Console.WriteLine($" {j} {k}");
                }
            }
        EXIT_LOOP: Console.WriteLine("suite");

            // goto avec un switch
            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    goto case 6;
                case 2:
                    Console.WriteLine("Mardi");
                    goto case 1;
                case 5:
                case 6:
                    Console.WriteLine("Week-end");
                    break;
                default:
                    Console.WriteLine("Week-end");
                    break;
            }
            #endregion

            #region Exercice Table de multiplication
            Console.WriteLine("Saisir un entier entre 1 et 9 pour afficher sa table de multiplication");
            for (; ; )
            {
                int m = Convert.ToInt32(Console.ReadLine());
                if (m < 1 || m > 10)
                {
                    break;
                }
                for (int j = 1; j < 10; j++)
                {
                    Console.WriteLine($"{m} x {j} = {m * j}");
                }
            }
            #endregion

            #region Exercice Quadrillage 
            int col = Convert.ToInt32(Console.ReadLine());
            int row = Convert.ToInt32(Console.ReadLine());
            for (int r = 0; r < row; r++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ]");
                }
                Console.WriteLine();
            }
            #endregion
        }
    }


}
