﻿namespace _09_libFicher
{
    public class FileUtil
    {
        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] lecteurs = DriveInfo.GetDrives();
            foreach (var drv in lecteurs)
            {
                if (drv.IsReady)     // permet de tester si le lecteur est prêt  
                {
                    Console.WriteLine(drv.Name);
                    Console.WriteLine(drv.TotalSize);       // si lelecteur n'est pas prêt -> exception 
                    Console.WriteLine(drv.TotalFreeSpace);  // ...
                    Console.WriteLine(drv.DriveType);       // ...
                    Console.WriteLine(drv.DriveFormat);     // idem
                }
                else
                {
                    Console.WriteLine($"{drv.Name} n'est pas pret ");
                }
            }
        }

        public static void InfoDossier()
        {
            //  Teste si le dossier existe
            if (Directory.Exists(@"C:\Dawan\TestIo"))
            {
                // Liste les répertoires contenu dans le chemin
                string[] rep = Directory.GetDirectories(@"C:\Dawan\TestIo");
                foreach (string r in rep)
                {
                    Console.WriteLine(r);
                }

                // Liste les fichiers du répertoire
                string[] fi = Directory.GetFiles(@"C:\Dawan\TestIo");
                foreach (string f in fi)
                {
                    Console.WriteLine(f);
                }
                if (Directory.Exists(@"C:\Dawan\TestIo\vide"))
                {
                    Directory.Delete(@"C:\Dawan\TestIo\vide");
                }
            }
            else
            {
                // Création du répertoire
                Directory.CreateDirectory(@"C:\Dawan\TestIo");
            }
        }

        // Exemple de fonction récursive pour parcourir en profondeur un dossier
        public static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] file = Directory.GetFiles(path);
                foreach (var f in file)
                {
                    Console.WriteLine(f);
                }
                string[] reps = Directory.GetDirectories(path);
                foreach (string rep in reps)
                {
                    Console.WriteLine($"Répertoire{rep}");
                    Console.WriteLine($"_______________");
                    Parcourir(rep);
                }
            }
        }

        public static void InfoFichier()
        {
            // Teste si le fichier
            if (File.Exists(@"C:\Dawan\TestIo\asup.txt"))
            {
                // Supprime le fichier
                File.Delete(@"C:\Dawan\TestIo\asup.txt");
            }
        }



        public static void EcrireFichierTexte(string path)
        {
            StreamWriter sw = null; // StreamWriter Ecrire un fichier texte
            try
            {
                sw = new StreamWriter(path, true);  // append à true => compléte le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Helloworld");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        public static List<string> LireFichierTexte(string path)
        {
            List<string> list = new List<string>();
            // Using => Équivalent d'un try / finally + Close()
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    list.Add(sr.ReadLine());
                }
            }
            return list;
        }
        public static void EcrireFichierBinaire(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))   // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 104; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBinaire(string path)
        {
            byte[] buf = new byte[10];
            int nb = 1;

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                while (nb != 0)
                {
                    nb = fs.Read(buf, 0, buf.Length);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    for (int i = 0; i < nb; i++)        // Read => retourne le nombre d'octets lue dans le fichier
                    {
                        Console.Write($"{buf[i]}");
                    }
                    Console.WriteLine();
                }
            }
        }


        // Copie d'un fichier binaire
        public static void Copie(string source, string cible)
        {
            // FileStream =>  permet de Lire/Ecrire un fichier binaire
            byte[] t = new byte[1000];
            FileStream fi = null;
            FileStream fo = null;
            try
            {
                fi = new FileStream(source, FileMode.Open);
                fo = new FileStream(cible, FileMode.CreateNew);
                while (true)
                {
                    // Lit 1000 octets au maximum dans le fichier (source) et les place dans le tableau à partir de l'indice 0
                    // Read => retourne le nombre d'octets lue dans le fichier
                    int size = fi.Read(t, 0, t.Length);
                    if (size == 0)
                    {
                        break;
                    }
                    // écrit dans le fichier (cible) les octets qui ont été lue
                    fo.Write(t, 0, size);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (fi != null)
                {
                    fi.Close();
                    fi.Dispose();
                }
                if (fo != null)
                {
                    fo.Close();
                    fo.Dispose();
                }

            }
        }

    }
}
