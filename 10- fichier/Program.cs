﻿using _09_libFicher;

namespace _10__fichier
{
    internal class Program
    {
        // Pour ajouter au projet la bibliothèque
        // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 09-LibFichier
        static void Main()
        {
            FileUtil.InfoLecteur();
            FileUtil.InfoDossier();
            FileUtil.InfoFichier();
            FileUtil.Parcourir(@"C:\Dawan\TestIo");

            // Fichier texte
            FileUtil.EcrireFichierTexte("test.txt");
            List<string> lst = FileUtil.LireFichierTexte("test.txt");
            foreach (string s in lst)
            {
                Console.WriteLine(s);
            }

            // Fichier binaire
            FileUtil.EcrireFichierBinaire(@"C:\Dawan\TestIo\Test.bin");
            FileUtil.LireFichierBinaire(@"C:\Dawan\TestIo\Test.bin");

            FileUtil.Copie(@"C:\Dawan\TestIo\Logo dawan.png", @"C:\Dawan\TestIo\test.png");


        }
    }
}
