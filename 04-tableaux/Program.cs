﻿namespace _04_tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimension
            // Déclarer un tableau
            double[] tab = new double[5];
            // double[] tab;
            // tab= new double[5];

            // Valeur d'nitialisation des éléments du tableau
            // Valeur d'nitialisation des éléments du tableau
            // entier -> 0
            // nombre réel -> 0.0
            // caractère -> '\u0000'
            // boolean -> false
            // référence -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[0]);
            tab[1] = 3.14;
            Console.WriteLine(tab[1]);

            // Si l'on essaye d'accéder à un élément en dehors du tableau -> IndexOutOfRangeException
            // Console.WriteLine(tab[10]); 

            // Nombre d'élément du tableau
            Console.WriteLine($"Nombre élément={tab.Length}");

            // Parcourir complétement un tableau (avec un for) 
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"{tab[i]} ");
            }

            // Parcourir complétement un tableau (foreach)
            // foreach (double elm in tab) // ou
            foreach (var elm in tab)
            {
                Console.WriteLine(elm);
                // elm uniquement en lecture
                // elm = 34.5;  // => Erreur ,on ne peut pas modifier elm
            }

            // Déclarer et initaliser un tableau
            char[] tabChr = { 'a', 'z', 'e', 'r' };
            foreach (var c in tabChr)
            {
                Console.WriteLine(c);
            }

            // On peut utiliser une variable entière pour définir la taille du tableau
            int size = 4;
            string[] tabStr = new string[size];

            foreach (var elm in tab)
            {
                Console.WriteLine(elm); // elm est uniquement en lecture 
                // elm = 5.8;
            }
            #endregion

            #region exercice Tableau
            // int[] t = { -7, -6, -4, -8, -3 };    // 1
            Console.Write("Taille du tableau=");
            int s = Convert.ToInt32(Console.ReadLine()); // 2
            int[] t = new int[s];
            for (int i = 0; i < s; i++)
            {
                t[i] = Convert.ToInt32(Console.ReadLine());
            }

            int max = t[0];  // int.MinValue
            double somme = 0.0;
            foreach (var e in t)
            {
                if (e > max)
                {
                    max = e;
                }
                somme += e;
            }
            Console.WriteLine($"Maximum={max} Moyenne={somme / tab.Length}");
            #endregion

            #region tableau à n dimension
            // Déclaration d'un tableau à 2 dimensions
            // Nombre maximum de dimmesion 32
            int[,] tab2d = new int[4, 2];

            // Accès à un élémént d'un tableau à 2 dimensions
            tab2d[0, 1] = 42;
            Console.WriteLine(tab2d[0, 1]);

            // Nombre d'élément du tableau
            Console.WriteLine(tab2d.Length);    // 8

            // Nombre de ligne
            Console.WriteLine(tab2d.GetLength(0));  // 4

            // Nombre de colonne
            Console.WriteLine(tab2d.GetLength(1));  // 2

            // Nombre de dimmension du tableau
            Console.WriteLine(tab2d.Rank); // 2

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tab2d.GetLength(0); i++)
            {
                for (int j = 0; j < tab2d.GetLength(1); j++)
                {
                    Console.Write($"{tab2d[i, j]}\t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var e in tab2d)
            {
                Console.WriteLine(e);
            }

            // Déclaration et initialisation tableau à 2 dimensions
            string[,] tabStr2d = { { "az", "er" }, { "rty", "uio" } };

            // Déclaration d'un tableau à 3 dimensions
            int[,,] tab3D = new int[3, 5, 6];
            #endregion

            #region Tableau en escalier
            // Déclaration d'un tableau de tableau
            double[][] tabEsc = new double[4][];
            tabEsc[0] = new double[4];
            tabEsc[1] = new double[2];
            tabEsc[2] = new double[3];
            tabEsc[3] = new double[2];

            // Accès à un élément
            tabEsc[2][2] = 4.5;
            Console.WriteLine(tabEsc[2][2]);

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length); // 4

            // Nombre de colonne par ligne
            foreach (var row in tabEsc)  // foreach(double[] e in tabEsc) 
            {
                Console.WriteLine(row.Length); // 4 2 3 2
            }

            // Parcourir complétement un tableau de tableau => for
            for (int l = 0; l < tabEsc.Length; l++)
            {
                for (int c = 0; c < tabEsc[l].Length; c++)
                {
                    Console.Write($"{tabEsc[l][c]}\t");
                }
                Console.WriteLine();
            }

            // Parcourir complétement un tableau de tableau => foreach
            foreach (double[] row in tabEsc)
            {
                foreach (double elm in row)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }

            // Déclarer et initialiser un tableau de tableau
            int[][] tabEscInit = {
                                    new int[] { 2, 3, 5 },
                                    new int[] { 5, 6 }
                                 };

            foreach (int[] row in tabEscInit)
            {
                foreach (int elm in row)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }
            #endregion

            #region indice et range C#8
            int[] t2 = { 1, 6, 4, 7, 3 };
            // Indice -> accéder à un élément d'un tableau à partir de la fin du tableau avec l'opérateur ^
            Console.WriteLine(t2[^1]); // 3

            // Range -> accéder à une plage du tableau
            // du début du tableau jusqu'à l'indice 3 (exclut) 
            int[] t3 = t2[..3]; // 1 6 4
            foreach (var e in t3)
            {
                Console.WriteLine(e);
            }

            // de l'indice 2 à la fin du tableau
            int[] t4 = t2[2..]; // 4 7 3
            foreach (var e in t4)
            {
                Console.WriteLine(e);
            }

            // de l'indice 1 jusqu'à l'indice 3 (exclut)
            int[] t5 = t2[1..3]; // 6 4
            foreach (var e in t5)
            {
                Console.WriteLine(e);
            }

            // de l'indice 2 à partir de la fin indice jusqu'à la fin du tableau
            int[] t6 = t2[^2..]; // 7 3
            foreach (var e in t6)
            {
                Console.WriteLine(e);
            }
            #endregion
        }
    }
}
