﻿using System.Text;

namespace _02_base
{
    // Une enumération est un ensemble de constante
    enum Motorisation { ESSENCE, DIESEL, GPL, ELECTRIQUE }

    // Par défaut, une énumération est de type int , on peut définir un autre type qui ne peut-être que de type intégral
    enum Direction : short { NORD = 90, OUEST = 180, EST = 0, SUD = 270 }

    // Énumération comme indicateurs binaires
    [Flags]
    enum JourSemaine
    {
        LUNDI = 1,
        MARDI = 2,
        MERCREDI = 4,
        JEUDI = 8,
        VENDREDI = 16,
        SAMEDI = 32,
        DIMANCHE = 64,
        WEEKEND = SAMEDI | DIMANCHE //96
    }

    // Définition d'une structure
    struct Point
    {
        public int x;
        public int y;
    }
    internal class Program
    {
        static void Main(string[] args)
        {
            #region declaration de variable

            // Déclaration d'une variable   type nomVariable;
            int j;

            //Console.WriteLine(i); // Erreur => on ne peut pas utiliser une variable locale non initialisée

            // Initialisation    nomVariable = valeur
            j = 42;

            // Déclaration et initialisation de variable 
            int l = 2;

            // Déclaration multiple de variable
            double largeur = 5, hauteur = 34;

            Console.WriteLine(j);

            Console.WriteLine(largeur + " hauteur=" + hauteur); // + => concaténation
            #endregion

            #region Littéral
            // Littéral booléen
            bool tst = false; //true
            Console.WriteLine(tst);

            // Littéral caractère
            char chr = 'a';
            char chrUnicode = '\u0045'; // Caractère en UTF-16
            char chrUnicodeHexa = '\x45';
            Console.WriteLine(chr + " " + chrUnicode + " " + chrUnicodeHexa);

            long m = 123L;  // L > long
            uint ui = 123U; // U -> unsigned
            Console.WriteLine(m + " " + ui);

            // Littéral entier -> changement de base
            int dec = 123;         // décimal (base 10) par défaut
            int hexa = 0xFF3054;   // 0x => héxadécimal (base 16)
            int bin = 0b01010;     // 0b => binaire (base 2)
            Console.WriteLine(dec + " " + hexa + " " + bin);

            // Littéral nombre à virgule flottante -> par défaut double
            float f = 12.3F;     // F -> float
            decimal d = 12.3M;   // M -> Decimal
            Console.WriteLine(f + " " + d);
            #endregion

            // Type implicite->var
            // Le type est déterminé par le type de la littérale, de l'expression ou du retour d'une méthode
            var imp1 = 10;      // imp1 -> int
            var imp2 = l + j;   // imp2 -> int
            var imp3 = dec + " " + hexa + " " + bin; // imp3 -> string
            Console.WriteLine(imp1 + " " + imp2 + " " + imp3);

            // avec @ on peut utiliser les mots réservés comme nom de variable (uniquement si nécessaire)
            int @while = 10;
            Console.WriteLine(@while);

            #region Transtypage
            // Transtypage autormatique (pas de perte de donnée)
            // type inférieur => type supérieur
            int ti1 = 10;
            double ti2 = ti1;
            long ti3 = ti1;
            Console.WriteLine(ti1 + " " + ti2 + " " + ti3);

            // Transtypage explicite: cast -> (nouveauType)
            double te1 = 3.14;
            int te2 = (int)te1;
            float te3 = (float)te1;
            decimal te4 = (decimal)te1;
            Console.WriteLine(te1 + " " + te2 + " " + te3 + " " + te4);

            // Dépassement de capacité
            int dep1 = 300;            // 00000000 00000000 00000001 00101100    300
            sbyte dep2 = (sbyte)dep1;  //                            00101100    44

            // checked => checked permet d’activer explicitement le contrôle de dépassement de capacité 
            // pour les conversions et les opérations arithmétiques de type intégral
            //checked
            //{
            //    dep2 = (sbyte)dep1; // avec checked une exception est générée, s'il y a un dépassement de capacité
            //}

            // Par défaut la vérification des dépassements de capacités est unchecked
            // si l'on veut qu'elles soient par défaut checked: il faut choisir dans les propriétés du projet:
            // Build -> Avancé -> rechercher un dépassement arithmétique -> cocher: lance des exceptions lorsque l'arithmétique ...
            // Dans ce cas, on utilise unchecked pour désactiver la vérification des dépassements
            //unchecked
            //{
            //    dep2 = (sbyte)dep1;  // plus de vérification de dépassement de capacité
            //}
            Console.WriteLine(dep1 + " " + dep2);
            #endregion

            #region Fonction de conversion
            // La classe Convert contient des méthodes statiques permettant de convertir un entier vers un double, ...
            // On peut aussi convertir une chaine de caractères en un type numérique
            int fc1 = Convert.ToInt32("1234");
            Console.WriteLine(fc1);
            double fc2 = Convert.ToDouble("1,2");
            Console.WriteLine(fc2);

            // int fc = Convert.ToInt32("azerty"); //  Erreur => génère une exception formatException

            // Conversion d'une chaine de caractères en entier
            // Parse
            int fc3 = int.Parse("345");
            // fc3 = int.Parse("azery");     //  Erreur => génère une exception formatException
            Console.WriteLine(fc3);
            #endregion

            #region Type référence
            StringBuilder sb1 = new StringBuilder("AZERTY");
            StringBuilder sb2 = null;
            Console.WriteLine(sb1 + " " + sb2);
            sb2 = sb1;
            Console.WriteLine(sb1 + " " + sb2);
            sb1 = null;
            Console.WriteLine(sb1 + " " + sb2);
            sb2 = null; // s1 et s2 sont égales à null
                        // Il n'y a plus de référence sur l'objet, il est éligible à la destruction par le garbage collector
            Console.WriteLine(sb1 + " " + sb2);
            #endregion

            #region Constante
            const double PI = 3.14;
            Console.WriteLine(PI);
            double pi2 = PI * 2;
            Console.WriteLine(pi2);
            // PI = 3.1419;     // Erreur: on ne peut pas modifier la valeur d'une constante
            //const int vr1;    // Erreur: on est obligé d'initialiser une constante
            #endregion

            #region Opérateur
            // opérateur arithmétique
            int a1 = 3;
            int a2 = 32;
            int res = a1 + a2;
            Console.WriteLine(res);     // 35
            Console.WriteLine(a1 % 2); //1

            // division 0
            // entier -> exception

            // int zero = 0;
            // Console.WriteLine(1/zero);

            // Nombre à virgule flottante
            Console.WriteLine(1.0 / 0.0); // double.PositiveInfinity
            Console.WriteLine(-1.0 / 0.0); // double.NegativeInfinity
            Console.WriteLine(0.0 / 0.0); // double.NaN

            // Incrémentation

            // pré-incrémentation
            int inc = 0;
            res = ++inc;    // inc =1 res=1
            Console.WriteLine(inc + " " + res);

            // post-incrémentation
            inc = 0;
            res = inc++;    // res=0 inc=1
            Console.WriteLine(inc + " " + res);

            // Affectation composé
            inc = 2;
            res = 10;
            res += inc; // conrespont à res=res+inc
            Console.WriteLine(res);

            // Opérateur de comparaison
            bool tst1 = res < 100;      // Une comparaison a pour résultat un booléen
            Console.WriteLine(tst1);    // true

            // Opérateur logique
            // Opérateur Non -> !
            bool tst2 = !tst1; // false
            Console.WriteLine(tst2);

            // a  b | et ou   ou exclusif
            //---------------------------   
            // f  f | f | f | f
            // v  f | f | v | v
            // f  v | f | v | v
            // v  v | v | v | f

            // Opérateur court-circuit && et ||
            // && -> dés qu'une comparaison est fausse, les suivantes ne sont pas évaluées
            bool tst3 = res > 100 && inc++ == 2; // inc++ n'est pas exécuté
            Console.WriteLine(tst3 + " " + inc); // false 2 

            // || -> dés qu'une comparaison est vrai, les suivantes ne sont pas évaluées
            bool tst4 = res == 35 || res > 100;
            Console.WriteLine(tst4);    // true

            // Opérateur Binaires (bit à bit)
            byte b = 0b11010;
            Console.WriteLine(Convert.ToString(~b, 2)); // Opérateur ~ => complémént: 1-> 0 et 0-> 1
            Console.WriteLine(Convert.ToString(b & 0b01001, 2)); // et => 01000
            Console.WriteLine(Convert.ToString(b | 0b01001, 2)); // ou =>11111
            Console.WriteLine(Convert.ToString(b ^ 0b01001, 2)); // ou exclusif => 10111

            // Decalage
            Console.WriteLine(Convert.ToString(b >> 1, 2)); // Décalage à droite de 1 (insertion de 1 0 à gauche)  => 1101
            Console.WriteLine(Convert.ToString(b << 2, 2)); // Décalage à gauche de 1 (insertion de 2 0 à droite)  => 1101000

            // L’opérateur de fusion null ?? retourne la valeur de l’opérande de gauche si elle n’est pas null
            // sinon, il évalue l’opérande de droite et retourne son résultat
            string str2 = "azerty";
            string strRes = str2 ?? "defaut";
            Console.WriteLine(strRes);  // azerty

            str2 = null;
            strRes = str2 ?? "defaut";
            Console.WriteLine(strRes);   // default
            #endregion

            #region Promotion numérique
            // Le type le + petit est promu vers le + grand type des deux
            int pr1 = 11;
            long pr2 = 12L;
            long pre = pr1 + pr2; // pr1 est promu en long

            double pr3 = 4.5;
            double pre2 = pr1 + pr3; // pr1 est promu en double

            double pr4 = pr1 / 2;   // un entier divisé par un entier donne un entier
            Console.WriteLine(pr4); // 5.0

            double pr5 = pr1 / 2.0; // on divise par double, pn1 est promu en double => la résultat est un double
            //     pr5 = =  ((double)pr1) / 2; // idem en convertissant avec un cast pr1 vers un double, 2 est promu en double 2.0
            Console.WriteLine(pr5); // 5.5

            // sbyte, byte, short, ushort, char sont promus en int
            short sh1 = 1;
            short sh2 = 2;
            int sh3 = sh1 + sh2; // sh1 et sh2 sont promu en int
            Console.WriteLine(sh3);
            #endregion

            #region Format chaine de caractère
            int xi = 3;
            int yi = 4;

            // 1
            string strFormat = string.Format("xi={0} yi={1}", xi, yi);  // On peut définir directement le format dans la mèthode WriteLine
            Console.WriteLine(strFormat);
            Console.WriteLine("xi={0} yi={1}", xi, yi);

            // 2
            Console.WriteLine($"xi={xi} yi={yi} xi+yi={xi + yi}");

            // 3
            Console.WriteLine("c:\tmp\newfile.txt"); // \t et \n sont considérés comme des caractères spéciaux

            // Caractères spéciaux
            // \n       Nouvelle ligne
            // \r       Retour chariot
            // \f       Saut de page
            // \t       Tabulation horizontale
            // \v       Tabulation verticale
            // \0       Caractère nul
            // \a 	    Alerte
            // \b       Retour arrière
            // \\       Backslash
            // \'       Apostrophe
            // \"       Guillemet

            Console.WriteLine(@"c:\tmp\newfile.txt"); // @ devant la littérale supprime l'interprétation dans la chaine des caractères spéciaux 

            Console.WriteLine("c:\\tmp\\newfile.txt"); // on peut arriver au même resultat en doublant les antislash
            #endregion

            #region Exercice Somme
            // Saisir 2 chiffres et afficher le résultat dans la console sous la forme 1 + 3 =  4
            Console.WriteLine("Somme: entrer 2 entiers");
            int s1 = Convert.ToInt32(Console.ReadLine());
            int s2 = Convert.ToInt32(Console.ReadLine());
            int somme = s1 + s2;
            Console.WriteLine($"{s1} + {s2} = {somme}");
            #endregion

            #region Exercice Moyenne
            // Saisir 2 nombres entiers et afficher la moyenne dans la console
            Console.WriteLine("Moyenne: entrer 2 entiers");
            int m1 = Convert.ToInt32(Console.ReadLine());
            int m2 = Convert.ToInt32(Console.ReadLine());
            double moyenne = (m1 + m2) / 2.0;
            Console.WriteLine($"Moyenne={moyenne}");
            #endregion

            #region nullable
            int? n = 4;
            if (n.HasValue)
            {
                // int h = (int)n* 2;
                int h = n.Value * 2;
                Console.WriteLine(h);
            }
            #endregion

            #region enumeration
            // ma est une variable qui ne pourra accepter que les valeurs de l'enum Motorisation
            Motorisation ma = Motorisation.ESSENCE;
            Console.WriteLine(ma);  // ESSENCE

            // enum -> string
            string mStr = ma.ToString();
            Console.WriteLine(mStr);    // ESSENCE

            // enum -> entier (cast)
            int iM = (int)ma;
            Console.WriteLine(iM);

            short dir = (short)Direction.SUD;
            Console.WriteLine(dir);

            // string -> enum
            // La méthode Parse permet de convertir une chaine de caractère en une constante enummérée
            Motorisation mb = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL");
            Console.WriteLine(mb);
            Console.WriteLine((short)mb);

            // m2 = (Motorisation)Enum.Parse(typeof(Motorisation), "GPL2");
            // Console.WriteLine(m);

            // entier -> enum
            int vm = 1;
            // Permet de tester si la valeur entière existe dans l'enumération
            if (Enum.IsDefined(typeof(Motorisation), vm))
            {
                Motorisation m3 = (Motorisation)vm;
                Console.WriteLine(m3);
            }

            // Énumération comme indicateurs binaires
            JourSemaine jour = JourSemaine.LUNDI | JourSemaine.MERCREDI;

            // Avec l'attribut [Flag] -> affiche LUNDI, MERCREDI , sans -> affiche 5
            Console.WriteLine(jour);

            if ((jour & JourSemaine.WEEKEND) != 0)  // teste la présence de WEEKEND
            {
                Console.WriteLine("Week-end");
            }
            else if ((jour & JourSemaine.MERCREDI) != 0)    // teste la présence de MERCREDI
            {
                Console.WriteLine("Mecredi");
            }

            // Utilisation d'une énumération avec switch
            switch (ma)
            {
                case Motorisation.ESSENCE:
                    Console.WriteLine("essence");
                    break;
                case Motorisation.DIESEL:
                    Console.WriteLine("diesel");
                    break;
                default:
                    Console.WriteLine("autre motorisation");
                    break;
            }
            #endregion
            #region structure
            Point p1;
            p1.x = 3;   // accès au champs x de la structure
            p1.y = 2;
            Console.WriteLine($"X={p1.X}  Y={p1.Y}");
            Console.WriteLine(p1);

            // Affectation d'une structure
            Point p2 = p1;  // Les champs de p2 sont initialisé avec les valeurs des champs de p1


            p2.x = 8;
            Console.WriteLine($"X={p1.x} {p1.y}");
            Console.WriteLine($"X={p2.x} {p2.y}");

            // Comparaison de 2 structures => Il faut comparer chaque champs
            if (p1.x == p2.x && p1.y == p2.y)
            {
                Console.WriteLine("égal");
            }
            #endregion
        }
    }
}
