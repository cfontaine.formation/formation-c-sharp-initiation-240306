﻿namespace _07_exception
{    // Exception personnalisée => on hérite de la classe Exception ou d'une classe enfant de Exception
    internal class AgeNegatifException : Exception
    {
        public AgeNegatifException()
        {
        }

        public AgeNegatifException(string message) : base(message)
        {

        }

        public AgeNegatifException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
