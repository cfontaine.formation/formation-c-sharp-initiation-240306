﻿namespace _07_exception
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[] tab = new int[5];
            try
            {
                int index = Convert.ToInt32(Console.ReadLine());    // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                Console.WriteLine(tab[index]);                      // Peut générer une exception IndexOutOfRangeException , si index est > à Length
                int age = Convert.ToInt32(Console.ReadLine());      // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitementEmploye(age);
                Console.WriteLine("Suite du programme");
            }
            catch (IndexOutOfRangeException e)      // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine("En dehors des limites du tableau");
                Console.WriteLine(e.Message);       // Message -> permet de récupérer le messsage de l'exception
            }
            catch (FormatException e)               // Attrape les exceptions FormatException
            {
                Console.WriteLine("Erreur de format");
                Console.WriteLine(e.StackTrace);
            }
            catch (Exception e)                     // Attrape tous les autres exception
            {
                Console.WriteLine("Autre exception");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);    // StackTrace -> affiche des informations sur la ligne où a été lancé l'exception  
            }
            finally // Le bloc finally est toujours éxécuté
            {
                // finally -> généralement utilisé pour libérer les ressources
                Console.WriteLine("Toujours exécuté");
            }
            Console.WriteLine("Fin du programme");
        }

        public static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début traitement employé");
            // Traitement partiel de l'exception AgeNegatifException
            try
            {
                TraitementAge(age);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Traitment exception");
                // throw;   // On relance l'exception pour que l'utilisateur de la méthode la traite à son niveau
                throw new Exception("Employe age négatif", ex); // On relance une exception différente
            }                                                   // e => référence à l'exception qui a provoquer l'exception
            Console.WriteLine("Fin traitement employé");
        }

        public static void TraitementAge(int age)
        {
            Console.WriteLine("Début traitement Age");
            if (age < 0)
            {
                throw new AgeNegatifException("Age négatif");  // throw => Lancer un exception
                                                               //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
            }                                           //  si elle n'est pas traiter, aprés la méthode Main => arrét du programme

            Console.WriteLine("Fin traitement Age");
        }
    }
}
