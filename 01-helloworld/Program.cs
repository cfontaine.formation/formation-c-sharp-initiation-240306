﻿/// <summary>
/// La classe Program
/// </summary>
internal class Program
{

    /// <summary>
    /// Point d'entrée du programme
    /// </summary>
    /// <param name="args">Arguments ligne de commande</param>
    private static void Main(string[] args)
    {
        /*
         * Commentaire
         * sur 
         * plusieurs
         * lignes
         */

        // commentaire fin de ligne
        Console.WriteLine("Hello, World!"); // commentaire fin de ligne
    }
}