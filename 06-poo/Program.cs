﻿namespace _06_poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Voiture v1=new Voiture();
            v1.Vitesse = 30;
            v1.Accelerer(20);
            v1.Afficher();
            v1.Freiner(10);
            Console.WriteLine(v1.EstArreter());
            v1.Arreter();


            Voiture v2=new Voiture();
            v2.Vitesse = 10;

            Voiture v3 = new Voiture("Opel", "vert");
            v3.Afficher();
        }
    }
}
