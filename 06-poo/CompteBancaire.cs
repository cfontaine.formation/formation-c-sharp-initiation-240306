﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_poo
{
    internal class CompteBancaire
    {
        public double Solde { get; private set; } = 50.0;
        
        public string Iban {  get; }

        public string Titulaire { get; set; }

        public CompteBancaire()
        {
        }

        public CompteBancaire(string titulaire)
        {
            Titulaire = titulaire;
        }

        public CompteBancaire(double solde, string titulaire)
        {
            Solde = solde;
            Titulaire = titulaire;
        }

        public void Crediter(double valeur)
        {
            if(valeur > 0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0)
            {
                Solde -= valeur;
            }
        }

        public bool EstPositif()
        {
            return Solde >= 0;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Titulaire} {Solde} {Iban}");
        }
    }
}
