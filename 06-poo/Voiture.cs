﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06_poo
{
    internal class Voiture
    {
     //   private string Marque = "Fiat";
    //    private string Couleur; // = default;
        private int _vitesse;
        private int _compteurKm = 20;

        public Voiture()
        {

        }

        public Voiture(string marque, string couleur)
        {
            Marque = marque;
            Couleur = couleur;
        }

        public Voiture(string marque, string couleur, int vitesse, int compteurKm) : this(marque, couleur)
        {
            Vitesse = vitesse;
            CompteurKm = compteurKm;
        }

       public int Vitesse
        {
            get
            {
                return _vitesse;
            }
            set
            {
                if(value > 0)
                {
                    _vitesse = value;
                }
            }
        }

        // C# 7.0
        public int CompteurKm
        {
            get => _compteurKm;
            set => _compteurKm = value;
        }
        public string Marque { get; private set; } = "Fiat";
        public string Couleur { get; set; }
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                Vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                Vitesse -= vFrn;
            }
        }

        public void Arreter()
        {
            Vitesse = 0;
        }

        public bool EstArreter()
        {
            return Vitesse == 0;
        }

        public void Afficher()
        {
            Console.WriteLine($"{Vitesse} {Marque} {Couleur} {CompteurKm}");
        }
    }
}
