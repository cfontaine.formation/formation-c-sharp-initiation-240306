﻿namespace _05_methodes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode
            double res = Multiplier(3.45, 6.78);
            Console.WriteLine(res);

            // Appel de methode (sans type retour)
            Afficher(3);

            #region Exercice Maximum
            Console.WriteLine("Maximum: entrer 2 entiers ");
            double d1 = Convert.ToDouble(Console.ReadLine());
            double d2 = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine($"Maximum={Maximum(d1, d2)}");
            #endregion

            #region Exercice Nombre pair
            Console.WriteLine(Even(6));
            Console.WriteLine(Even(3));
            #endregion

            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int val = 10;
            TestParamValeur(val);
            Console.WriteLine(val);

            // Passage de paramètre par référence
            // La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamRef(ref val);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // int r1,r2;
            // On peut déclarer les variables de retours dans les arguments pendant l'appel de la méthode
            TestParamOut(out int r1, out int r2);
            Console.WriteLine($"{r1} {r2}");

            // On peut ignorer un paramètre out en le nommant _
            TestParamOut(out _, out int r3);
            Console.WriteLine(r3);

            // Passage par référence avec in (C# 7.2)
            TestParamIn(in val);
            TestParamIn(val);

            // paramètre optionnel
            TestParamOptionnel(5);                  // 5    true    hello
            TestParamOptionnel(7, false);            // 7    false   hello
            TestParamOptionnel(7, false, "azerty");  // 7    false   azerty

            // Paramètres nommés
            TestParamOptionnel(i: 4, s: "qwerty");
            TestParamOptionnel(s: "qwerty", i: 4, b: false);
            TestParamOut(b: out int v2, a: out _);

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(10));
            Console.WriteLine(Moyenne(10, 14, 20, 9));
            int[] n = { 2, 4, 9, 1 };
            Console.WriteLine(Moyenne(11, n));

            #region Exercice echange
            int a = 2;
            int b = 8;
            Console.WriteLine($"a={a} b={b}");
            Swap(ref a, ref b);
            Console.WriteLine($"a={a} b={b}");
            #endregion

            #region
            Console.WriteLine(EstPresent('A', 'e', 'r', 't'));
            Console.WriteLine(EstPresent('A', 'e', 'A', 't')); // true
            Console.WriteLine(EstPresent('A'));
            #endregion

            #region Surcharge de méthode
            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 2));         // => appel de la méthode avec 2 int en paramètres
            Console.WriteLine(Somme(1, 2.5));       // => appel de la méthode avec 1 entier et 1 double en paramètre
            Console.WriteLine(Somme(1.6, 2.5));     // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme("aze", "rty")); // => appel de la méthode avec 2 chaines en paramètre

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(1, 2L));        // => appel de la méthode avec 1 entier et 1 double en paramètre
            Console.WriteLine(Somme('a', 'b'));     // => appel de la méthode avec 2 int en paramètres
            Console.WriteLine(Somme(0.5f, 0.6f));   // => appel de la méthode avec 2 double en paramètres

            // Pas de conversion possible => Erreur
            // Console.WriteLine(Somme(0.5M, 0.6M));
            #endregion

            AfficherSquare(4);
            AfficherSquare2(4);
            AfficherSquare3(4);
            AfficherSquare4(4);
            int fac = Factorial(3);

            foreach(var par in args)
            {
                Console.WriteLine(par);
            }
        }

        // Déclaration
        static double Multiplier(double d1, double d2)
        {
            return d1 * d2;  // L'instruction return
                             // - Interrompt l'exécution de la méthode
                             // - Retourne la valeur de l'expression à droite
        }

        static void Afficher(int a) // void => pas de valeur retourné
        {
            Console.WriteLine(a);
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        static double Maximum(double a, double b)
        {
            return a < b ? b : a;
            //if(a < b)
            //{
            //    return b;
            //}
            //else
            //{
            //    return a;
            //}
        }
        #endregion

        #region Exercice Paire
        static bool Even(int v)
        {
            return (v & 0b1) == 0;
            //return v % 2 == 0;
            //if (v % 2 == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        #endregion

        #region Passage de paramètre
        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 23; // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamRef(ref int a)
        {
            Console.WriteLine(a);
            a = 23;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamOut(out int a, out int b)
        {
            // Console.WriteLine(a);    // erreur, on ne peut pas accéder en entrée à un paramètre out 
            a = 23;
            Console.WriteLine(a);
            b = a * 2;                  // La méthode doit obligatoirement affecter une valeur aux paramètres out
        }

        // Passage de paramètre par référence en lecture seule -> in
        static void TestParamIn(in int i)
        {
            Console.WriteLine(i);
            // i = 4;    // Dans la méthode on ne peut pas modifier un paramètre in
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(int i, bool b = true, string s = "hello")
        {
            Console.WriteLine($"i={i} b={b} s={s}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int n, params int[] notes)
        {
            double somme = n;
            foreach (var e in notes)
            {
                somme += e;
            }
            return somme / (notes.Length + 1);
        }
        #endregion

        #region Exercice Echange
        static void Swap(ref int i1, ref int i2)
        {
            int tmp = i1;
            i1 = i2;
            i2 = tmp;
        }
        #endregion

        static bool EstPresent(char chrSearch, params char[] val)
        {
            foreach (var elm in val)
            {
                if (elm == chrSearch)
                {
                    return true;
                }
            }
            return false;
        }

        #region Surcharge de méthode
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }
        static int Somme(int c, int d, int e)
        {
            Console.WriteLine("3 entiers");
            return c + d + e;
        }

        static double Somme(int a, double b)
        {
            Console.WriteLine("un entier et un double");
            return a + b;
        }
        static double Somme(double a, double b)
        {
            Console.WriteLine("2 double");
            return a + b;
        }

        static string Somme(string s1, string s2)
        {
            Console.WriteLine("2 chaines de caractères");
            return s1 + s2;
        }
        #endregion

        #region Corps d'expression
        // une méthode qui comprend une seule expression, peut s'écrire avec l'opérateur =>
        static int Multiplier(int a, int b) => a * b; // avec les Corps d'expression return est implicite

        static void Afficher(double a) => Console.WriteLine(a);
        #endregion

        #region Méthode locale
        static void AfficherSquare(double v)
        {
            Console.WriteLine(Square(v));

            // Une méthode locale n'est visible que par la méthode englobante
            double Square(double d) => d * d;
        }

        // Capture
        // Une méthode locale peut accéder
        // - aux paramètres de la méthode englobante
        static void AfficherSquare2(double v)
        {


            Console.WriteLine(Square());

            double Square() => v * v;
        }
        // -  aux variables locales de la méthode englobante
        static void AfficherSquare3(double v)
        {
            double w = v;
            Console.WriteLine(Square());

            double Square() => w * w;
        }
        // 
        static void AfficherSquare4(double v)
        {
            Console.WriteLine(Square(v));

            // Une méthode locale static ne peut pas accéder 
            // aux paramètres et aux variables locales de la méthode englobante
            static double Square(double d) => d * d;
        }
        #endregion
        #region Recursivité
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1)// condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }
        #endregion
    }
}